#!perl -T

use Test::More tests => 1;

BEGIN {
	use_ok( 'Net::GlobalPOPs::MediaServicesAPI' );
}

diag( "Testing Net::GlobalPOPs::MediaServicesAPI $Net::GlobalPOPs::MediaServicesAPI::VERSION, Perl $], $^X" );
