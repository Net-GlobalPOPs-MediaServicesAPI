#!/usr/bin/perl -w

#BEGIN {
#  $Net::HTTPS::Any::skip_NetSSLeay = 1;
#  $Net::HTTPS::Any::skip_NetSSLeay = 1;
#}

use Test::More;

require 't/lib/test_account.pl';
require Net::GlobalPOPs::MediaServicesAPI;

my($login, $password) = test_account_or_skip();
plan tests => 1;

my $debug = $ENV{TEST_VERBOSE};

my $gp = Net::GlobalPOPs::MediaServicesAPI->new( 'login'    => $login,
                                                 'password' => $password,
                                                 #'debug'    => $debug,
                                               );
my $return = $gp->getDID();

use Data::Dumper;
diag( Dumper($return) ) if $debug;

#XXX test some things about the return...
ok( $return, 'getDID returned something' );
