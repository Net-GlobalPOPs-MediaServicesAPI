#!/usr/bin/perl -w

use Test::More skip_all => "not working with current test account?";

require 't/lib/test_account.pl';
require Net::GlobalPOPs::MediaServicesAPI;

my($login, $password) = test_account_or_skip();
plan tests => 3;

my $debug = $ENV{TEST_VERBOSE};

my $gp = Net::GlobalPOPs::MediaServicesAPI->new( 'login'    => $login,
                                                 'password' => $password,
                                                 #'debug'    => $debug,
                                               );
my $return = $gp->auditDIDs();

use Data::Dumper;
diag( Dumper($return) ); # if $debug;

ok( $return, 'auditDIDs returned something' );

#         'did' => [
#                   {
#                     'statuscode' => '100',
#                     'status' => 'Number currently assigned to you with refid  rewritten as \'\' to endpoint 521',
#                     'cnam' => 'False',
#                     'expire_seconds' => {},
#                     'availability' => 'assigned',
#                     'tn' => '2137851430',
#                     'rewrite' => {},
#                     'endpoint' => '521',
#                     'refid' => {}
#                   },
#                   {
#                     'statuscode' => '100',
#                     'status' => 'Number currently assigned to you with refid  rewritten as \'\' to endpoint 521',
#                     'cnam' => 'False',
#                     'expire_seconds' => {},
#                     'availability' => 'assigned',
#                     'tn' => '5109626974',
#                     'rewrite' => {},
#                     'endpoint' => '521',
#                     'refid' => {}
#                   }
#                 ]


ok( $return->{'did'}, 'auditDIDs returned did data' );

ok( ref($return->{'did'}) eq 'ARRAY', 'auditDIDs did data is a list' );

