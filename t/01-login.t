#!/usr/bin/perl -w

use Test::More;
require 't/lib/test_account.pl';

my($login, $password) = test_account_or_skip;
plan tests => 2;

use_ok 'Net::GlobalPOPs::MediaServicesAPI';

my $gp = Net::GlobalPOPs::MediaServicesAPI->new('login'    => $login,
                                                'password' => $password );

#eww.  what is the right, TEST_VERBOSE-aware way to do this
warn Net::GlobalPOPs::MediaServicesAPI->errstr unless $gp;

ok( $gp, 'Login sucessful' );
