use Test::More;

sub test_account_or_skip {
    my($login, $password) = test_account();

    unless( defined $login ) {
        plan skip_all => "No test account";
    }

    return($login, $password);
}

sub test_account {
    open TEST_ACCOUNT, "t/test_account" or return;
    my($login, $password, $hostport) = <TEST_ACCOUNT>;
    chomp $login;
    chomp $password;
    chomp $hostport;

    if ( $hostport =~ /^\s*([\w\.]+)(:(\d+))?\s*$/ ) {
      my($host, $port) = ($1, $3);
      $Net::GlobalPOPs::MediaServicesAPI::HOST = $host;
      $Net::GlobalPOPs::MediaServicesAPI::PORT = $port;
    }

    return($login, $password);
}

1;
