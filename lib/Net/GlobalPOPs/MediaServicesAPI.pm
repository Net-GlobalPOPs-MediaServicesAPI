package Net::GlobalPOPs::MediaServicesAPI;

use warnings;
use strict;
use Data::Dumper;
use XML::Simple;
use XML::Writer;
use Net::HTTPS::Any qw( 0.10 https_post );

=head1 NAME

Net::GlobalPOPs::MediaServicesAPI - Interface to GlobalPOPs Media Services API

=cut

our $VERSION = '0.03';
our $URL     = 'https://www.loginto.us/VOIP/api.pl';
#could be parsed from URL, if it mattered...
our $HOST    = 'www.loginto.us';
our $PATH    = '/VOIP/api.pl';
our $PORT    = 443; #to allow testing to override

our $AUTOLOAD;
our $errstr = '';

=head1 SYNOPSIS

    use Net::GlobalPOPs::MediaServicesAPI;

    my $handle = Net::GlobalPOPs::MediaServicesAPI->new(
        'login'    => 'tofu',
        'password' => 'beast',
        'debug'    => 1,
    );

    #DID functions
    #auditDIDs
    #queryDID
    #reserveDID
    #assignDID
    #configDID
    #releaseDID

    #911 Functions

    #Locator Functions

    ...

=head1 METHODS

=head2 new HASHREF | OPTION, VALUE ...

Creates a new Net::GlobalPOPs::MediaServicesAPI object.  Options may be passed
as a hash reference or a flat list of names and values.

=over 4

=item login (required)

=item password (required)

=item login (required)

=back

=cut

#  If there is an error,
#returns false and sets an error string which may be queried with the I<errstr>
#class method.

sub new {
  my $proto = shift;
  my $class = ref($proto) || $proto;
  my $self = ref($_[0]) ? shift : { @_ };
  $self->{'debug'} ||= 0;
  bless($self, $class);
}

=head2 errstr

=cut

sub errstr {
  my $class = shift;

  return $errstr
    unless ref($class) && $class->isa('Net::GlobalPOPs::MediaServicesAPI');

  my $self = $class;
  $self->{errstr};
}

sub DESTROY { }; # no-op

sub AUTOLOAD {
  my $self = shift;
  my $opts = ref($_[0]) ? shift : { @_ };

  $AUTOLOAD =~ /(^|::)(\w+)$/ or die "unparsable AUTOLOAD: $AUTOLOAD";
  my $function = $2;

  my $output;
  my $w = new XML::Writer(OUTPUT => \$output, DATA_MODE => 1, DATA_INDENT => 3);

  $w->xmlDecl('ISO-8859-1');
  $w->doctype('request', undef, $URL);

  $w->startTag('request', 'id' => '');  #XXX request ID???
    $w->startTag('header');
      $w->startTag('sender');

        if ( $self->{'sessionid'} ) {

          #$w->dataElement( 'login'     => ''                   );
          #$w->dataElement( 'password'  => ''                   );
          $w->dataElement( 'sessionid' => $self->{'sessionid'} );

        } else {

          $w->dataElement( 'login'     => $self->{'login'}     );
          $w->dataElement( 'password'  => $self->{'password'}  );
          #$w->dataElement( 'sessionid' => ''                   );

        }

      $w->endTag('sender');
    $w->endTag('header');

    $w->startTag('body');

      $w->dataElement( 'requesttype' => $function );

      if ( keys %$opts ) {
        $w->startTag('item');
          foreach my $opt ( keys %$opts ) {
            $w->dataElement( $opt => $opts->{$opt} );
          }
        $w->endTag('item');
      }

    $w->endTag('body');

  $w->endTag('request');

  #$output =~ s/\n+/\n/g;

  warn "XML Request for $function function:\n$output"
    if $self->{'debug'} > 1;

  my( $page, $response, %reply_headers ) = https_post(
    'host'         => $HOST,
    'port'         => $PORT,
    'path'         => ($PATH||443),
    'args'         => { 'apidata' => $output, },
    #'content'      => $output,
    #'Content-Type' => 'text/plain',
    #'Content-Type' => 'text/xml',
    #'Content-Type' => 'application/xml',
    #'headers' => {},
    'debug'        => ( $self->{'debug'} > 1 ? 1 : 0 ),
  );

  unless ( $response =~ /^200/i ) {
    $self->{'errstr'} = $response;
    return '';
  }

  warn "XML Response for $function function:\n: $page"
    if $self->{'debug'} > 1;

  my $hashref = XMLin( $page );

  warn "Parsed response for $function funtion:\n". Dumper($hashref)
    if $self->{'debug'} > 1;

  my $return = $hashref->{'body'};

  warn "Returning data:\n". Dumper($return)
    if $self->{'debug'} > 1;

  $return;

}

=head1 AUTHOR

Ivan Kohler, C<< <ivan-net-globalpops at freeside.biz> >>

=head1 BUGS

Please report any bugs or feature requests to C<bug-net-globalpops-mediaservicesapi at rt.cpan.org>, or through
the web interface at L<http://rt.cpan.org/NoAuth/ReportBug.html?Queue=Net-GlobalPOPs-MediaServicesAPI>.  I will be notified, and then you'll
automatically be notified of progress on your bug as I make changes.




=head1 SUPPORT

You can find documentation for this module with the perldoc command.

    perldoc Net::GlobalPOPs::MediaServicesAPI


You can also look for information at:

=over 4

=item * RT: CPAN's request tracker

L<http://rt.cpan.org/NoAuth/Bugs.html?Dist=Net-GlobalPOPs-MediaServicesAPI>

=item * AnnoCPAN: Annotated CPAN documentation

L<http://annocpan.org/dist/Net-GlobalPOPs-MediaServicesAPI>

=item * CPAN Ratings

L<http://cpanratings.perl.org/d/Net-GlobalPOPs-MediaServicesAPI>

=item * Search CPAN

L<http://search.cpan.org/dist/Net-GlobalPOPs-MediaServicesAPI>

=back


=head1 ACKNOWLEDGEMENTS


=head1 COPYRIGHT & LICENSE

Copyright 2008 Freeside Internet Services, Inc. (http://freeside.biz/)

This program is free software; you can redistribute it and/or modify it
under the same terms as Perl itself.


=head1 ADVERTISEMENT

Open-source billing, trouble ticketing and automation for VoIP providers:

http://freeside.biz/freeside/

=cut

1; # End of Net::GlobalPOPs::MediaServicesAPI
